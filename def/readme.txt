-ANIMALS-

In the animals definition file, each line defines an animal:

Biome Health Energy Speed Power Dangerous Name Loot,
...

The last line does not have an ending comma! Only Animal_Name and Loot_Object are strings,
which can be marked with '_' to indicate spaces (so a Polar Bear in the game is represented
as Polar_Bear in the definitions). The other values are integers except 'Dangerous', which
can be 0, 1 or 2, indicating if the animal is passive, semihostile and hostile.

-PLANTS-

The plant definitions is as follows:

Biome Energy Venomous Name Loot,
...

The last line does not have an ending comma! Name and Loot are strings, which work as in the
Animal definitions. Biome and Energy are integers, and Venomous can be 1 or 0.