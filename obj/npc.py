from random  import randint
import random
import time

class Npc(object):
    def __init__(self, n, z):

        # Set a name
        self.name = n
        # The zone in which the NPC is
        self.zone = z
        # Set a type:
        #   0 -> Bandit
        #   1 -> Trader
        #   2 -> Stander
        # The type is set by zones:
        n = randint(0, 9)

        # Suburbia, 70% chance of Stander, 20% Trader, 10% Bandit
        if self.zone == 0:
            if   n <= 6:
                self.type = 2
            elif n <= 8:
                self.type = 1
            else:
                self.type = 0

        # Market, 60% chance of Trader, 40% Stander
        elif self.zone == 1:
            if n <= 5:
                self.type = 1
            else:
                self.type = 2

        # Streets, 90% chance of Stander, 10% Trader
        else:
            if n <= 8:
                self.type = 2
            else:
                self.type = 1


        # Streets, 90% chance of Stander, 10% Trader
        # Create an inventory
        self.inv = []
        # Create a known variable, which tracks if the player has previously
        # spoken with this NPC
        self.known = False
        # For DEV purposes
        #print("Hi! I am "+self.name+", of type "+str(self.type))


    # NPC CIS
    def talkToNPC(self, p):

        # The NPC greeting changes if the player already knows the NPC or not
        if self.known == False:
            print(">" + self.name + ": Hi! My name is "
                      + self.name + ", nice to meet you.")
            # Now that you have spoken to them, then you know them
            known = True
        else:
            print(">" + self.name + ": Hi " + p.name + ", what's up?")
    
        talking = True
        while talking and p.isAlive():
            
            # Get the player input
            print("")
            cmd = input("<" + p.name + ": ").lower()
            
            # Exit the Npc CIS
            if cmd == "exit":
                print("You finish the conversation and get back to the city.")
                talking = False
                break

            # Get some help
            if cmd == "help":
                print(
                    "exit:  stop talking with " + self.name +"\n" \
                    "stat:  show your stats\n" \
                    "look:  look around you" \
                )
            
            # Take a look at the NPC
            if cmd == "look":
               
                s = "You are talking to " + self.name
                # Add the type of NPC if it's not Stander 
                if self.type == 0:
                    s = s + ", who seems to be a bandit."
                elif self.type == 1:
                    s = s + ", who seems to be a trader."
                else:
                    s = s + "."
                # Finally, print the message
                print(s)
