class Player(object):

    # Constructor
    def __init__(self, n):

        # Position
        self.x = 0
        self.y = 0
        # Stats
        self.name   = n
        self.cash   = 0
        self.health = 5
        self.energy = 5
        self.power  = 1
        self.speed  = 1
        self.inv = ["Paper", "Knife", "Potato", "Map"]
        print("You are born, your name is " + self.name + ".")
        # Things so the player knows when it changes biomes
        self.prevBiome = 8

    # Add an element to the inventory
    def addInv(self, o):
        self.inv.append(str(o))
        print(str(o) + " is now in your inventory.")

    # Remove an element from the inventory. Returns True if it has been successfully
    # removed, and False if it does not exist
    def remInv(self, o):
        if str(o) in self.inv:
            self.inv.remove(str(o))
            print(str(o) + " is no longer in your inventory.")
            return True
        else:
            print("You do not have any " + str(o) + ".") 
            return False
        
    # Check for an item in the inventory
    def checkInv(self, o):
        if str(o) in self.inv:
            return True
        else:
            return False

    # Get a look at your stats
    def getStat(self):

        # If the inventory is empty
        if len(self.inv) == 0:
            s = "Your inventory is empty."

        # If you have one element
        elif len(self.inv) == 1:
            s = "You have " + self.inv[0] + "."

        elif len(self.inv) == 2:
            s = "You have " + self.inv[0] + " and " + self.inv[1] + "."

        # If it has more than 2 elements
        else:
            # The string to be printed starts by:
            s = "You have "
            # Now we add all the contents of the
            # inventory but the last one
            for i in range(0, len(self.inv) - 1):
                s = s + self.inv[i] + ", "
            # Add the last element
            s = s + "and " + self.inv[len(self.inv) - 1] + "."

        # Now tell the cash of the player
        s = s + " Your purse contains " + str(self.cash) + "$."
        # The health
        s = s + " Your health is "
        if   self.health == 1:
            s = s + "really poor"
        elif self.health == 2:
            s = s + "bad"
        elif self.health == 3:
            s = s + "good"
        elif self.health == 4:
            s = s + "very good"
        elif self.health == 5:
            s = s + "full"
        # The energy
        s = s + ", and your energy level is " + str(self.energy) + "."
        # Finally return the thing
        return s

        
    # Move the player
    def moveUp(self, w):
        xPrev = self.x
        yPrev = self.y
        # If not in the border of the map
        if self.y > 0:
            self.y += -1
        else:
            # Teleport to the bottom of the map
            self.y = w.y - 1
        # If after moving you are not in the sea,
        # then the movement is correct
        if w.space[self.x][self.y].biome != 7:
            print("You move to the North.")
            self.changeEnergy(-2)
        else:
            print("You cannot walk into the sea.")
            self.x = xPrev
            self.y = yPrev

    def moveDown(self, w):
        xPrev = self.x
        yPrev = self.y
        # If not in the border of the map
        if self.y < w.y - 1:
            self.y += 1
        else:
            # Teleport to the top part of the map
            self.y = 0
        # If after moving you are not in the sea,
        # then the movement is correct
        if w.space[self.x][self.y].biome != 7:
            print("You move to the South.")
            self.changeEnergy(-2)
        else:
            print("You cannot walk into the sea.")
            self.x = xPrev
            self.y = yPrev

    def moveRight(self, w):
        xPrev = self.x
        yPrev = self.y
        # If not in the border of the map
        if self.x < w.x - 1:
            self.x += 1
        else:
            # Teleport to the leftmost part of the map
            self.x = 0
        # If after moving you are not in the sea,
        # then the movement is correct
        if w.space[self.x][self.y].biome != 7:
            print("You move to the East.")
            self.changeEnergy(-2)     
        else:
            print("You cannot walk into the sea.")
            self.x = xPrev
            self.y = yPrev

    def moveLeft(self ,w):
        xPrev = self.x
        yPrev = self.y
        # If not in the border of the map
        if self.x > 0:
            self.x += -1
        else:
            # Teleport to the rightmost part of the map
            self.x = w.x - 1
        # If after moving you are not in the sea,
        # then the movement is correct
        if w.space[self.x][self.y].biome != 7:
            print("You move to the West.")
            self.changeEnergy(-2)
        else:
            print("You cannot walk into the sea.")
            self.x = xPrev
            self.y = yPrev

        
    #Manage the energy
    def changeEnergy(self, n):
        self.energy = self.energy + n
        if n < 0:
            print("Your energy level decreases by " + str(n*-1) +" point(s).")
        else:
            print("Your energy level increases by " + str(n) + " point(s).")


    # Restore naturally the energy levels
    def restoreEnergy(self):
        self.energy = 5
        print("Your energy levels are restored.")

    # Kill the player
    def die(self):
        self.health = 0
        
    # See if the player is alive
    def isAlive(self):
        if self.health > 0:
            return True
        else:
            return False