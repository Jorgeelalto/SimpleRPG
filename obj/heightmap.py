import random
import math
from random import randint

class Heightmap(object):


    # The constructor takes the dimensions of the heightmap,
    # being the number of cells in X, in Y and the maximum
    # height.
    def __init__(self, x, y, z):
        
        # Some variable initialization
        self.xDim = x
        self.yDim = y
        self.zDim = z
        self.space = []
        self.max = 0
        
        # Finish creating the matrix "space"
        for i in range(0, self.xDim):
            # Add the columns (y) to the row (x)
            self.space.append([])
            # In each position of that column,
            for j in range(0, self.yDim):
                # Add an integer zero
                self.space[i].append(0)


    # Generates a hill at a given point with certain radius.
    # As you make the radius larger, the hill gets higher
    def genHill(self, x, y, rad):
    
        # In each column,
        for i in range(0, self.xDim):
            # And in each position of the columns,
            for j in range(0, self.yDim):
                # Generate the height of that point
                z = int(rad**2 - ((x - i)**2 + (y - j)**2))
                # Only add the new height if it's adding, not substracting
                if z > 0:
                    # This only works with self.space[i][j] since
                    # you are assigning, not just reading
                    self.space[i][j] += z
                    # Update the maximum value for the global height
                    if self.space[i][j] > self.max:
                        self.max = self.space[i][j]



    # Makes the tallest hill's height coincide with the maximum
    # height specified in the constructor. Really useful after
    # generating a full and detailed heightmap
    def normalize(self):
        # In each column,
        for i in range(0, self.xDim):
            # And in each position of the columns,
            for j in range(0, self.yDim):
                # Normalize (scale) that value with respect to the recorded
                # maximum and the desired maximum height for the map
                self.space[i][j] = int((self.space[i][j] / self.max)**2 * self.zDim)

            
    # Returns the height in a given position
    def getHeightAt(self, x, y):
        return self.space[x - 1][y - 1]


    # Generates a full detailed map.
    def genFullMap(self):

        # Big mountains
        for i in range(0, int(math.sqrt((self.xDim**2 + self.yDim**2)) / 9)):
            self.genHill(randint(0, self.xDim), randint(0, self.yDim), randint(10, 14))
        # Medium hills
        for i in range(0, int(math.sqrt((self.xDim**2 + self.yDim**2)))):
            self.genHill(randint(0, self.xDim), randint(0, self.yDim), randint(4, 9))
        # Terrain roughness
        for i in range(0, int(math.sqrt((self.xDim**2 + self.yDim**2)) * 10)):
            self.genHill(randint(0, self.xDim), randint(0, self.yDim), randint(2, 3))
        
        # After generating the hills, it's time to normalize
        self.normalize()
        
        # And after normalizing, we can add some noise
        # For each point,
        for i in range(0, self.xDim):
            for j in range(0, self.yDim):
                # If its height is bigger than one (so small seas are not created),
                if self.space[i][j] > 1:
                    # Then we may randomly substract one
                    self.space[i][j] += random.choice((-1, 0))
                    
    
    # Prints the heightmap to the screen, for dev purposes
    def show(self):
        # In each column,
        for i in range(0, self.xDim):
            # And in each position of the columns,
            for j in range(0, self.yDim):
                # Normalize the printed height to two figures' numbers
                if self.space[i][j] < 10:
                    print("0" + str(self.space[i][j]), end="")
                else:
                    print(str(self.space[i][j]), end="")
            print("")