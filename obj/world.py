from random import randint
from obj.animal      import Animal
from obj.city        import City
from obj.cell        import Cell
from obj.terraformer import Terraformer
from obj.parser      import Parser
import sys
import random


class World(object):

    def __init__(self, x, y, z):

        # Space
        self.x = x
        self.y = y
        self.z = z
        # Creating the list of lists for the matrix space
        self.space = []
        # World generation
        print("Creating the world...")
        # We fill the space list with more lists
        for i in range(0, self.x):
            self.space.append([])
            # And in each of those lists we put Cells
            for j in range(0, self.y):
                self.space[i].append(Cell(i, j))

        # The map of the space is now generated with the Terraformer
        t = Terraformer()
        # The terraformer needs to read the animal definitions from a file
        pr = Parser()
        print("Creating life...")
        self.animalDef = pr.getAnimals()
        self.plantDef  = pr.getPlants()
        t.generateMap(self.x, self.y, self.z, self.space, self.animalDef, self.plantDef)
        # Time
        self.year = 0
        self.month = 1
        self.day = 1
        # Some final printing
        print("The world is now finished. It's so big, beautiful and full of secrets!")

    # Returns the current time of the world as a human-readable string
    def gettimestring(self):
        return str(self.day) + "/" + str(self.month) + "/" + str(self.year)

    # Adds a day to the world time. This allows us to manage the months and years
    def sumday(self):

        # If we are not at the end of the month
        if self.day < 30:
            self.day += 1
            return
        # If we are
        else:
            # If it's also the end of the year
            if self.month == 12:
                self.year += 1
                self.month = 1
                self.day = 1
                return
            # If we reach here it's because it's the end of the month
            self.month += 1
            self.day = 1

    # Sets the player in a random position inside the map
    def setplayer(self, p):
        
        # Search for a valid position if the current one is Sea
        while self.space[p.x][p.y].biome == 7:
            p.x = int(self.x * (randint(0, 99) * 0.01))
            p.y = int(self.y * (randint(0, 99) * 0.01))
        

    # Prints the map in the screen, with special symbols for each cell
    def showMap(self, player):
        
        # Empty space for GUI eyecandy
        print("")
        # First line is the upper edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # New line
        print("")
        
        for i in range(0, self.y):
            # First, the edge of the line
            print("| ", end="")
            # Now, each cell of our line i
            for j in range(0, self.x):
                # Get a shortcut for accessing the cells biome
                # since we are reading from it only, not writing
                cell = self.space[j][i].biome
                # Print the player position if it corresponds
                if player.x == j and player.y == i:
                    print("[]", end="")
                # And only print the cells if they have been discovered
                elif self.space[j][i].discovered == False:
                    print("  ", end="")
                # Plains
                elif cell == 0:
                    print("--", end="")
                # Forest
                elif cell == 1:
                    print(random.choice(("+!", "!+", "!!", "++")), end="")
                # Artic
                elif cell == 2:
                    print(random.choice((".*", "*.", "**")), end="")
                # Jungle
                elif cell == 3:
                    print(random.choice(("%#", "#%", "##", "%%")), end="")
                # Desert
                elif cell == 4:
                    print("~~", end="")
                # River
                elif cell == 5:
                    print(random.choice((";;", "::", ":;", ";:")), end="")
                # Mountains
                elif cell == 6:
                    print(random.choice(("/^", "\\^", "^\\", "^/")), end="")
                # Sea
                elif cell == 7:
                    print(random.choice((".,", ",.", "..", ",,")), end="")
            # End of our line i, we print the edge
            print(" |")
            
        # Last line is the bottom edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # Empty space for GUI eyecandy
        print("")
    
    # Prints the map in the screen, with special symbols for each cell
    def showDevMap(self, player):
        
        # Biome map
        print("\nBiomes")
        # Empty space for GUI eyecandy
        print("")
        # First line is the upper edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # New line
        print("")
        # The cells of the map
        for i in range(0, self.y):
            # First, the edge of the line
            print("| ", end="")
            # Now, each cell of our line i
            for j in range(0, self.x):
                # Get a shortcut for accessing the cells biome
                # since we are reading from it only, not writing
                cell = self.space[j][i].biome
                # Print the player position if it corresponds
                if player.x == j and player.y == i:
                    print("[]", end="")
                # Plains
                elif cell == 0:
                    print("--", end="")
                # Forest
                elif cell == 1:
                    print("++", end="")
                # Artic
                elif cell == 2:
                    print("**", end="")
                # Jungle
                elif cell == 3:
                    print("##", end="")
                # Desert
                elif cell == 4:
                    print("~~", end="")
                # River
                elif cell == 5:
                    print("::", end="")
                # Mountains
                elif cell == 6:
                    print("^^", end="")
                # Sea
                elif cell == 7:
                    print(",,", end="")
            # End of our line i, we print the edge
            print(" |")
            
        # Last line is the bottom edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # Empty space for GUI eyecandy
        print("")
        
        # Heightmap
        print("Heights")
        # Empty space for GUI eyecandy
        print("")
        # First line is the upper edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # New line
        print("")
        # The cells of the map
        for i in range(0, self.y):
            # First, the edge of the line
            print("| ", end="")
            # Now, each cell of our line i
            for j in range(0, self.x):
                # If the height has two digits
                if self.space[j][i].height > 9:
                    print(str(self.space[j][i].height), end="")
                else:
                    print(" " + str(self.space[j][i].height), end="")
            # End of our line i, we print the edge
            print(" |")
        # Last line is the bottom edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # Empty space for GUI eyecandy
        print("")
    
        # Weather map
        print("Weather")
        # Empty space for GUI eyecandy
        print("")
        # First line is the upper edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # New line
        print("")
        # The cells of the map
        for i in range(0, self.y):
            # First, the edge of the line
            print("| ", end="")
            # Now, each cell of our line i
            for j in range(0, self.x):
                print("."+str(self.space[j][i].weather), end="")
            # End of our line i, we print the edge
            print(" |")
        # Last line is the bottom edge
        for i in range(0, self.y * 2 + 4):
            print("-", end="")
        # Empty space for GUI eyecandy
        print("")


    # Vary the weather naturally
    def varyWeather(self):
        print("Adding and changing weather: 0%", end="\r")
        for i in range(0, self.x):
            for j in range(0, self.y):
                
                # Get the weather so I can work better
                weather = self.space[i][j].weather
                
                # Plains
                if   self.space[i][j].biome == 0:
                    # Clear -> cloudy 33%
                    if   weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 1
                    # Clear -> fog 33%
                    elif weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 8
                    # Cloudy -> light rain 100%
                    elif weather == 1:
                        self.space[i][j].weather = 2
                    # light rain -> rain 33%
                    elif weather == 2 and randint(0, 2) == 0:
                        self.space[i][j].weather = 3
                    # rain -> storm 33%
                    elif weather == 3 and randint(0, 2) == 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 50%
                    elif weather == 4 and randint(0, 1) == 0:
                        self.space[i][j].weather = 5
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
                    
                # Forest
                elif self.space[i][j].biome == 1:
                    # Clear -> cloudy 33%
                    if   weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 1
                    # Clear -> fog 33%
                    elif weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 8
                    # Cloudy -> light rain 66%
                    elif weather == 1 and randint(0, 2) != 0:
                        self.space[i][j].weather = 2
                    # light rain -> rain 25%
                    elif weather == 2 and randint(0, 3) == 0:
                        self.space[i][j].weather = 3
                    # rain -> storm 25%
                    elif weather == 3 and randint(0, 3) == 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 50%
                    elif weather == 4 and randint(0, 1) == 0:
                        self.space[i][j].weather = 5
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
                
                # Artic
                elif self.space[i][j].biome == 2:
                    # Clear -> cloudy 50%
                    if   weather == 0 and randint(0, 1) == 0:
                        self.space[i][j].weather = 1
                    # Cloudy -> snow 80%
                    elif weather == 1 and randint(0, 4) != 0:
                        self.space[i][j].weather = 7
                    # Snow -> snow 50%
                    elif weather == 7 and randint(0, 1) == 0:
                        self.space[i][j].weather = 7
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
                        
                # Jungle
                elif self.space[i][j].biome == 3:
                    # Clear -> ligh rain 66%
                    if weather == 0 and randint(0, 2) != 0:
                        self.space[i][j].weather = 2
                    # Light rain -> rain 66%
                    elif weather == 2 and randint(0, 2) != 0:
                        self.space[i][j].weather = 3
                    # Rain -> storm 33%
                    elif weather == 3 and randint(0, 2) == 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 33%
                    elif weather == 3 and randint(0, 2) == 0:
                        self.space[i][j].weather = 5
                    # storm -> hurricane 33%
                    elif weather == 3 and randint(0, 2) == 0:
                        self.space[i][j].weather = 6
                    # clear -> fog 33%
                    elif weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 8
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0

                # Desert
                elif self.space[i][j].biome == 4:
                    # Clear -> ligh rain 20%
                    if weather == 0 and randint(0, 4) == 0:
                        self.space[i][j].weather = 2
                    # Light rain -> rain 33%
                    elif weather == 2 and randint(0, 2) == 0:
                        self.space[i][j].weather = 3
                    # clear -> sandstorm 50%
                    elif weather == 0 and randint(0, 1) == 0:
                        self.space[i][j].weather = 9
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
                
                # River
                elif self.space[i][j].biome == 5:
                    # Clear -> cloudy 33%
                    if   weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 1
                    # Clear -> fog 66%
                    elif weather == 0 and randint(0, 2) != 0:
                        self.space[i][j].weather = 8
                    # Cloudy -> light rain 100%
                    elif weather == 1:
                        self.space[i][j].weather = 2
                    # light rain -> rain 33%
                    elif weather == 2 and randint(0, 2) == 0:
                        self.space[i][j].weather = 3
                    # rain -> storm 33%
                    elif weather == 3 and randint(0, 2) == 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 50%
                    elif weather == 4 and randint(0, 1) == 0:
                        self.space[i][j].weather = 5
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
 
                # Mountains
                elif self.space[i][j].biome == 6:
                    # Clear -> ligh rain 33%
                    if weather == 0 and randint(0, 2) == 0:
                        self.space[i][j].weather = 2
                    # Light rain -> rain 50%
                    elif weather == 2 and randint(0, 1) == 0:
                        self.space[i][j].weather = 3
                    # Rain -> storm 75%
                    elif weather == 3 and randint(0, 3) != 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 75%
                    elif weather == 3 and randint(0, 4) != 0:
                        self.space[i][j].weather = 5
                    # light rain -> fog 33%
                    elif weather == 1 and randint(0, 2) == 0:
                        self.space[i][j].weather = 8
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
                
                # Sea
                else:
                    # Clear -> cloudy 20%
                    if   weather == 0 and randint(0, 4) == 0:
                        self.space[i][j].weather = 1
                    # Clear -> fog 50%
                    elif weather == 0 and randint(0, 1) == 0:
                        self.space[i][j].weather = 8
                    # Cloudy -> light rain 50%
                    elif weather == 1 and randint(0, 1) == 0:
                        self.space[i][j].weather = 2
                    # light rain -> rain 66%
                    elif weather == 2 and randint(0, 2) != 0:
                        self.space[i][j].weather = 3
                    # rain -> storm 75%
                    elif weather == 3 and randint(0, 3) != 0:
                        self.space[i][j].weather = 4
                    # storm -> thunderstorm 33%
                    elif weather == 4 and randint(0, 2) == 0:
                        self.space[i][j].weather = 5
                    # If not, then clear
                    else:
                        self.space[i][j].weather = 0
    
    
    # Vary temperatures naturally
    def varyTemperatures(self):
        for i in range(0, self.x):
            for j in range(0, self.y):
                # Not every day the temperature changes
                if randint(0, 1) == 0:
                    # And there is also the possibility in which the change
                    # of temperature is only one degree celsius
                    if randint(0, 2) == 0:
                        self.space[i][j].temp += random.choice((-1, 1))
                    # Otherwise, the temperature is fully reset
                    # Plains
                    elif self.space[i][j].biome == 0:
                        # If we are near the north or south poles
                        if i < ( self.y/10) or i > ( self.y -  self.y/10):
                            self.space[i][j].temp = randint(10, 15)
                        else:
                            self.space[i][j].temp = randint(15, 25)
                    # Forest
                    elif self.space[i][j].biome == 1:
                        # If we are near the north or south poles
                        if i < ( self.y/10) or i > int( self.y -  self.y/10):
                            self.space[i][j].temp = randint(5, 10)
                        else:
                            self.space[i][j].temp = randint(10, 25)
                    # Artic
                    elif self.space[i][j].biome == 2:
                        # Artic will always be in the poles
                        self.space[i][j].temp = randint(-15, 0)
                    # Jungle
                    elif self.space[i][j].biome == 3:
                        # If we are a bit far from the equator
                        if i < ( self.y/4) or i > ( self.y -  self.y/4):
                            self.space[i][j].temp = randint(15, 25)
                        else:
                            self.space[i][j].temp = randint(25, 30)
                    # Desert
                    elif self.space[i][j].biome == 4:
                        # If we are near the north or south poles
                        if i < ( self.y/10) or i > ( self.y -  self.y/10):
                            self.space[i][j].temp = randint(20, 30)
                        else:
                            self.space[i][j].temp = randint(25, 40)
                    # River
                    elif self.space[i][j].biome == 5:
                        # If we are a bit far from the equator
                        if i < ( self.y/5) or i > ( self.y -  self.y/5):
                            self.space[i][j].temp = randint(5, 15)
                        else:
                            self.space[i][j].temp = randint(15, 20)
                    # Mountains
                    if self.space[i][j].biome == 6:
                        # Here, depending on the height, the ranges vary
                        if self.space[i][j].height >  self.z - ( self.z / 15):
                            # This is the tallest points of the mountains
                            self.space[i][j].temp = randint(-10, -5)
                        else:
                            self.space[i][j].temp = randint(-5, 10)
                    # Sea
                    else:
                        # If we are a bit far from the equator
                        if i < ( self.y/5) or i > ( self.y -  self.y/5):
                            self.space[i][j].temp = randint(5, 15)
                        else:
                            self.space[i][j].temp = randint(15, 25)