class Animal(object):
    def __init__(self, biome):

        # The biome in which the animal is
        self.biome = biome
        # The stats of the animal
        self.name   = ""    # String
        self.health = 0     # 0 to 5
        self.energy = 0     # 0 to 5
        self.speed  = 0     # 0 to 5
        self.power  = 0     # 0 to 5
        self.dangerous = 0  # 0 to 2: passive, semihostile, hostile
        self.loot = ""      # The stuff the player gets after hunting
        # For DEV purposes
        #print("Created a " + self.name + " at " + str(self.x) + ", " + str(self.y))
