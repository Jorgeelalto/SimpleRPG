from random import randint
from obj.city   import City
from obj.animal import Animal
from obj.fight  import Fight
import time
import random


class Cell(object):
    
    def __init__(self, x, y):

        # Set the position of the cell in the map
        self.x, self.y = x, y
        # The height will be changed using the Terraformer
        self.z = 32
        # If the cell has a city, it will be linked here
        self.city = None
        # The biome is indicated here
        self.biome = 0
        # There is a temperature in each cell, determined by the biome
        self.temp = 25
        # And there is also a weather
        self.weather = 0
        # There can be more than one animal on each cell
        self.animals = []
        # But there is only one animal the player can face
        # This stores the index of the animal faced
        self.animal = 0
        self.thereIsAnimal = True
        # The same but with plants, there is a list of plants in the cell,
        self.plants = []
        self.thereIsPlant = True
        # And there is the currently harvestable plant
        self.plant = 0
        # The Fight object so the player can hunt
        self.f = Fight()
        # If the cell has been discovered by the player or not
        self.discovered = False
        # The item the player threw away. It disappears when wandering around 
        self.thrownItem = 'None'

    # Returns a string, describing the environment on a tile and the distant looks
    # of the adjacent ones
    def getLookAt(self):
        # Change the description based on the biome passed with the parameters
        if   (self.biome == 0):
            # The biome here is Plains.
            d = "There is grass everywhere, and some bushes here and there, " \
                "but the trees are not abundant.\nYou are in the Plains."
        elif (self.biome == 1):
            #Forest
            d = "The trees are big and tall, the soil is fertile and everything " \
                "is full of brush.\nYou are in a Forest."
        elif (self.biome == 2):
            #Artic
            d = "It is too cold for most plants to grow. Everything is covered in " \
                "ice and snow.\nYou are in the Artic."
        elif (self.biome == 3):
            #Jungle
            d = "The humidity is high and brush is overgrown, even the tallest " \
                "trees cannot avoid the rainforest.\nYou are in the Jungle."
        elif (self.biome == 4):
            #Desert
            d = "The heat is too high for anything but cacti. Red stone stacks " \
                "itself in big and complex formations, and the sand covers almost " \
                "everything.\nYou are in the Desert."
        elif (self.biome == 5):
            #River
            d = "The water flows and its sound stands out of the monotony from the " \
                "other environments. Life is abundant here.\nYou are in a River."
        elif (self.biome == 6):
            # Mountains
            d = "Everything is covered with fallen rocks and patches of moss. It is " \
                "too cold and breathing is hard.\nYou are in the Mountains."

        elif (self.biome == 7):
            # Sea
            d = "You should be drowning, not reading this, but OK."

        # Now we check for cities
        if (self.city != None):
            d += "\nIt looks like there is a city, called "+self.city.name+"."
        # For animals
        if self.thereIsAnimal:
            d += "\nThere are signs of "+self.animals[self.animal].name+", too."
            
        # For plants
        if self.thereIsPlant:
            d += "\nYou can see some "+self.plants[self.plant].name+" around."
        
        # Now, for the weather
        # Clear
        if self.weather == 0:
            d += "\nThe day is clear."
        # Cloudy
        if self.weather == 1:
            d += "\nThere are some clouds up there."
        # Light rain
        if self.weather == 2:
            d += "\nThere is a very light rain."
        # Rain
        if self.weather == 3:
            d += "\nIt is raining."
        # Storm
        if self.weather == 4:
            d += "\nThere is a storm."
        # Thunderstorm
        if self.weather == 5:
            d += "\nThere is a thunderstorm."
        # Hurricane
        if self.weather == 6:
            d += "\nA hurricane is striking here."
        # Snow
        if self.weather == 7:
            d += "\nIt is snowing."
        # Fog
        if self.weather == 8:
            d += "\nThere is some fog."
        # Sandstorm
        if self.weather == 9:
            d += "\nA sandstorm is striking."
        
        # for the temperature
        if self.temp < -5:
            d += "\nIt is also deadly freezing."
        elif self.temp < 0:
            d += "\nIt is freezing here, too."
        elif self.temp < 7:
            d += "\nIt is also very cold around here."
        elif self.temp < 15:
            d += "\nIt is cold."
        elif self.temp < 20:
            d += "\nIt is also a bit cold around here."
        elif self.temp < 25:
            d += "\nIt is warm."
        elif self.temp < 30:
            d += "\nIt is also a bit hot."
        elif self.temp < 35:
            d += "\nIt is pretty hot around here."
        else:
            d += "\nIt is also deadly hot."
        
        # Finally, return the completed string
        return d
        
    
    # Gets the player into the biome, to interact with it and the animals
    def enterCell(self, p, w):
        
        # The player discovers the cell when they enter it
        self.discovered = True
        
        # Each time the player enters, there is a different animal near him
        # Only choose a new animal if there is any left
        if len(self.animals) > 0:
            # Choose a random animal
            self.animal = randint(0, len(self.animals) - 1)
            # Tell that there is an animal to this program
            self.thereIsAnimal = True
        # Each time the player enters, there is a different plant near him
        # Only choose a new animal if there is any left
        if len(self.plants) > 0:
            # Choose a random animal
            self.plant = randint(0, len(self.plants) - 1)
            # Tell that there is an animal to this program
            self.thereIsPlant = True
        
        # If you change of biome after moving, then print this info
        if p.prevBiome != self.biome:
            if   (self.biome == 0):
                # The biome here is Plains.
                print("You are in the Plains.")
            elif (self.biome == 1):
                #Forest
                print("You are in a Forest.")
            elif (self.biome == 2):
                #Artic
                print("You are in the Artic.")
            elif (self.biome == 3):
                #Jungle
                print("You are in the Jungle.")
            elif (self.biome == 4):
                #Desert
                print("You are in the Desert.")
            elif (self.biome == 5):
                #River
                print("You are in a River.")
            elif (self.biome == 6):
                # Mountains
                print("You are in the Mountains.")
        # Do stuff so this works
        p.prevBiome = self.biome

        # CIS loop
        while True:
            
            # Get the player command
            print("")
            cmd = input("{" + p.name + ": ").lower()
            
            
            # COMMON COMMANDS
            
            # Exit the game
            if cmd in ("exit", "leave", "leave world", "exit game", "end game"):
                print("You left the world.")
                return cmd

            # Display some help info about the possible commands
            elif cmd in ("help", "see help", "help me", "i'm dying"):
                print(
                    "exit: leave the game\n" \
                    "look: gather some information about the surroundings\n" \
                    "stat: show your stats\n" \
                    "enter city: enter a city if there is one around\n" \
                    "hunt: fight with animals to get loot\n" \
                    "harvest: well, that's it. Get stuff from plants\n" \
                    "sleep: get some rest and recover your energy\n" \
                    "move [n, s, e or w]: resume your journey\n" \
                    "wander: walk around, hopefully finding new things\n" \
                    "dig: make a little hole and get some materials\n" \
                    "get from ground: take grass, snow or similar\n" \
                    "throw item away: discard an item from your inventory\n" \
                    "get item back: recover the last discarded item from the ground"
                    )

                
            # Take a loot at the world
            elif cmd in ("look", "take a look", "get info"):
                print(self.getLookAt())

            # Print the status of the player, including health, energy, inventory and more
            elif cmd in ("stat", "stats", "status", "se status"):
                print(p.getStat())
                
            # Sleep - time passes and the player recovers energy
            elif cmd in ("sleep", "go to sleep", "rest", "go to rest", "take a nap"):
                
                # If energy levels are low enough
                if (p.energy < 3):
                    print("Sleeping",    end="\r")
                    time.sleep(1)
                    print("Sleeping.",   end="\r")
                    time.sleep(1)
                    print("Sleeping..",  end="\r")
                    time.sleep(1)
                    print("Sleeping...", end="\r")
                    time.sleep(1)
                    print("Sleeping...")
                    # Time passes
                    w.sumday()
                    # The temperature of the world changes
                    w.varyTemperatures()
                    # The weather changes too
                    w.varyWeather()
                    # And the energy is set to 3 again
                    p.restoreEnergy()
                    print("You get up. It's " + w.gettimestring() + ".")
                else:
                    print("You are too awake to sleep.")

            # Show the map of the world, if you have one
            elif cmd in ("map", "show map", "look map"):
                if "Map" in p.inv:
                    w.showMap(p)
                else:
                    print("You do not have a map.")
            
            elif cmd == "devmap":
                w.showDevMap(p)
            
            # Throw an item from the inventory away, discard it
            elif cmd in ("throw away", "throw item", "discard item", "discard"):
                # First get the item the player wants to discard
                i = input("What item do you want to discard?: ")
                if p.remInv(i):
                    thrownItem = i
            
            # Get the discarded item back. It is like if the player were getting it from the
            # ground after throwing it. They won't be able to do it after they wander around
            # since they won't know where it was.
            elif cmd in ("recover item", "get item back"):
                # If the player discarded an item
                if not thrownItem == 'None':
                    # Then add it again to the inventory
                    print("You found the last item you throw away.")
                    p.addInv(thrownItem)
                else:
                    print("There is nothing to recover here.")
            
            
            # SPECIFIC COMMANDS
            
            # More inmediate movements
            elif cmd in ("move n", "move north", "go north", "walk north"):
                if p.energy > 1:
                    # North
                    return "n"
                else:
                    print("You are too tired to continue your journey.\n" \
                        "Try sleeping a bit.")
                    
            elif cmd in ("move s", "move south", "go south", "walk south"):
                if p.energy > 1:
                    # South
                    return "s"
                else:
                    print("You are too tired to continue your journey.\n" \
                        "Try sleeping a bit.")
            
            elif cmd in ("move e", "move east", "go east", "walk east"):
                if p.energy > 1:
                    # East
                    return "e"
                else:
                    print("You are too tired to continue your journey.\n" \
                        "Try sleeping a bit.")
                        
            elif cmd in ("move w", "move west", "go west", "walk west"):
                if p.energy > 1:
                    # West
                    return "w"
                else:
                    print("You are too tired to continue your journey.\n" \
                        "Try sleeping a bit.")

            # Enter the city of the cell
            elif cmd in ("enter city", "go to city"):
                # If there is a city in the cell of the player
                if self.city != None:
                    # Then enter it
                    self.city.enterCity(p)
                # If there is no city
                else:
                    # Display an error message
                    print("There are no cities here.")
            
            # Hunt the animal you can see
            elif cmd in ("hunt", "hunt animal"):
                # At least one point of energy needed to hunt
                if p.energy > 0:
                    # Only hunt if there is something to hunt, obviously
                    if not self.thereIsAnimal:
                        print("There is nothing to hunt here.")
                    else:
                        result = self.f.doFight(p, self.animals[self.animal], self.animals[self.animal].loot)
                        # If the player loses the fight with the animal of the cell
                        if result == "":
                            # The player dies
                            p.die()
                            return
                        # If the player wins,
                        else:
                            # then the loot is added to the inventory,
                            p.addInv(result)
			    # the player loses a point of energy
                            p.changeEnergy(-1)
                            # and the animal dies
                            self.animals.pop(self.animal)
                            # You already fought in this wandering
                            self.thereIsAnimal = False
                # If you do not have enough energy
                else:
                    print("You are too tired to hunt.")

            # Harvesting plants
            elif cmd in ("harvest", "recollect", "harvest plant", "recollect plant"):
                
                # Only harvest if there is something to harvest, obviously
                if not self.thereIsPlant:
                    print("There is nothing to harvest here.")
                else:
                    # Get the loot of the plant
                    p.addInv(self.plants[self.plant].loot)
                    # If the plant is venomous
                    if self.plants[self.plant].venomous:
                        print("You got poisoned by some " + self.plants[self.plant].name + ".")
                        # If the player has enough health to survive
                        if p.health > 1:
                            # Just substract one point of health
                            p.health += -1
                        # If they can not survive, then kill them
                        else:
                            p.die()
                            return
                            
                    # There is a chance the plant gets removed
                    if randint(0, 2) == 0:
                        print("There is no more " + self.plants[self.plant].name + " left here.")
                        self.plants.pop(self.plant)
                        self.thereIsPlant = False
                        
            
            # Getting stuff from the floor
            elif cmd in ("get from the ground", "get from ground", "take from ground", "take from the ground"):
                print("Searching...")
                time.sleep(1)
                # Plains
                if self.biome == 0:
                    p.addInv("Grass")
                # Forest
                elif self.biome == 1:
                    p.addInv("Leaf")
                # Artic
                elif self.biome == 2:
                    p.addInv("Snow")
                # Jungle
                elif self.biome == 3:
                    p.addInv("Compost")
                # Desert
                elif self.biome == 4:
                    p.addInv("Sand")
                # River
                elif self.biome == 5:
                    p.addInv("Peeble")
                # Mountains
                elif self.biome == 6:
                    p.addInv("Stone")
                # Sea (you can't, sorry)
            
            # Digging and getting materials
            elif cmd in ("dig", "dig down", "dig a hole"):
                print("You dig a little hole in the ground.")
                time.sleep(1)
                # Get an item depending on the biome
                # Plains
                if self.biome == 0:
                    p.addInv("Dirt")
                # Forest
                elif self.biome == 1:
                    p.addInv("Dirt")
                # Artic
                elif self.biome == 2:
                    p.addInv("Snow")
                # Jungle
                elif self.biome == 3:
                    p.addInv("Soil")
                # Desert
                elif self.biome == 4:
                    p.addInv("Soil")
                # River
                elif self.biome == 5:
                    p.addInv("Mud")
                # Mountains
                elif self.biome == 6:
                    p.addInv("Stone")
                # Sea (you can't, sorry)

            
            # Wandering around changes the stuff that surrounds you (animals and plants)
            elif cmd in ("wander", "wander around", "walk around"):
                
                # Change the animal if there is any
                if len(self.animals) > 0:
                    # Choose a random animal
                    self.animal = randint(0, len(self.animals) - 1)
                    # Tell that there is an animal to this program
                    self.thereIsAnimal = True
                # Change the plant if there is any
                if len(self.plants) > 0:
                    # Choose a random animal
                    self.plant = randint(0, len(self.plants) - 1)
                    # Tell that there is an animal to this program
                    self.thereIsPlant = True
                # Delete the thrown item since the player can't get back to the same zone
                thrownItem = None

                print("Wandering around", end="\r")
                time.sleep(0.3)
                print("Wandering around.", end="\r")
                time.sleep(0.3)
                print("Wandering around..", end="\r")
                time.sleep(0.3)
                print("Wandering around...", end="\r")
                time.sleep(0.3)
                print("Wandering around...")
                

            # If the command is not valid
            else:
                print("The command is not valid. Try \'help\'.")
