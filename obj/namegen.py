from random import randint
import random


class NameGen(object):

    def __init__(self):

        # The constructor generates the vocal and consonant lists
        self.vocal       = ("a", "e", "i", "o", "u")
        self.consonant   = ("q", "r", "t", "y", "p", "s", "d", "f", "g", "h", "j", "l", "z", "c", "v", "b", "n", "m")
  
    # Generates a name adequate for a city, taking the maximum length of the word
    def getCityName(self, maxLength):

        # Get the length of the word (in syllables)
        length = randint(1, maxLength)
        # Initializing the string to be returned
        word = ""
        # This will track the last type of syllable added
        last = 0
        # n will determine if we add vocal+consonant,
        # consonant + vocal or vocal + vocal. The first time
        # the loop is executed n has to have a randomized value
        n = randint(0, 2)

        for i in range(0, length):

            # Adding a vocal + consonant
            if (n == 0):
                word = word + random.choice(self.vocal) + random.choice(self.consonant)
                n = randint(0, 2)
                last = 0
                continue
            # Adding consonant + vocal
            if (n == 1):
                word = word + random.choice(self.consonant) + random.choice(self.vocal)
                n = randint(0, 2)
                last = 1
                continue
            # Adding two consecutive vocals
            if (n == 2):
                word = word + self.getDoubleVocal()
                # Now we don't want to add more than two vocals
                n = 1
                last = 1

        # 20% chance of adding a final consonant to the name. Only add it if
        # the last letter added is a vocal
        if (last == 1):
            if (length > 1 and randint(0, 4) == 0) or (length == 1 and randint(0,1) == 0):
                word = word + random.choice(self.consonant)
        # Return the name with the first letter capitalized
        return word.title()
  
    # Generates NPCs names
    def getNpcName(self):

        # Initializing the word string to be returned
        word = ""
        # Adding the name
        word = word + self.getCityName(3)
        # Choosing the "internames" word
        inter = [" ", " of ", " Ben ", " from ", " van "]
        word = word + random.choice(inter)
        # Finally, adding the surname
        word = word + self.getCityName(4)
        # Return the full name
        return word

    # Generates a double vocal, used in the class only
    def getDoubleVocal(self):
    
        # First get a vocal
        first = random.choice(self.vocal)
        # Then, try to get a second one
        second = random.choice(self.vocal)
        # Since the second one has to be different from the first,
        while (second == first):
            # Choose another second vocal until it's different from the first
            second = random.choice(self.vocal)
        # And return the string with the two joined vocals
        return first + second