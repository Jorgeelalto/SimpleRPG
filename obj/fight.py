from random import randint
import time


class Fight(object):

    # This method returns a string (the reward) if the player wins the combat,
    # and an empty string if they lose.

    def doFight(self, player, enemy, reward):

        print("It's time to fight! - " + player.name + " vs " + enemy.name)
        # If True, then it's the player turn, if False, the enemy is playing.
        turn = True
        # While both fighers are alive
        while player.health > 0 and enemy.health > 0:

            # When it's player's turn
            if turn:
                
                # Temporary 20% chance of critical
                attack = player.power + randint(0, 1)
                enemy.health -= attack
                print(player.name + " inflicts " + str(attack) + " points of damage.")
                turn = False
            
            else:

                attack = enemy.power + randint(0, 1)
                player.health -= attack
                print(enemy.name + " inflicts " + str(attack) + " points of damage.")
                turn = True

            time.sleep(1)

        # When the combat finishes
        # If the player wins
        if player.health > 0:
            print(player.name + " wins!")
            time.sleep(1)
            return reward
        # If the player loses
        else:
            return ""
