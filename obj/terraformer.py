from random  import randint
from obj.animal    import Animal
from obj.city      import City
from obj.namegen   import NameGen
from obj.heightmap import Heightmap
import random
import math


class Terraformer(object):

    # Generates random biomes in each cell, and correspondant animals
    def generateMap(self, x, y, z, space, anDef, plDef):
        
        # We first generate a heightmap with the desired dimensions
        print("Creating the Z dimension of the map...")
        h = Heightmap(x, y, z)
        
        # And fill it with terrain
        print("Generating hills, oceans and plains...")
        h.genFullMap()
        
        # We now get the height of each point and set it to the cells
        print("Adding biomes: 0%", end="\r")
        # For each point,
        for i in range(0, x):
            for j in range(0, y):
                # We get the height of that point in the heightmap 
                space[i][j].height = h.getHeightAt(i, j)
                # Sea in the lowest level
                if   space[i][j].height == 0:
                    space[i][j].biome = 7
                # Plains, jungle and desert, between 1 and z / 3
                elif space[i][j].height < int(z / 3):
                    space[i][j].biome = random.choice((0, 3, 4))
                # Forest and jungle, between z / 3 and 3z / 4
                # Forest are 300% more frequent than jungle in this layer
                elif space[i][j].height < int((4*z) / 5):
                    space[i][j].biome = random.choice((1, 1, 1, 3))
                # Mountains from 3z / 4 upwards
                else:
                    space[i][j].biome = 6
                    
                # Then, if in the poles there is land, we change it for
                # Artic or Forest
                # If we are in the poles of the map and there is not sea,
                if   (j == 0 or j == y - 1) and space[i][j].biome != 7:
                    # Then set the Artic biome
                    space[i][j].biome = 2
                # Else, if we are near the poles and there is not sea,
                elif (j == 1 or j == y - 2) and space[i][j].biome != 7:
                    # Set randomly Artic or Forest
                    space[i][j].biome = random.choice((1, 2))
                    
            # Progress percentage
            print("Adding biomes: " + str(int(i * 60 / x)) + "%", end="\r")
        # Progress percentage for finished
        print("Adding biomes: 100%")


        # Rivers
        print("Filling rivers...")
        # Repeat this loop as many times as rivers you want to add
        for i in range(0, int(math.sqrt((x/7)**2 + (y/7)**2))):
            # We get a starting point for the river, we have to
            # find a valid value for x and for y (land)
            rx = randint(0, x - 1)
            ry = randint(0, y - 1)
            while space[rx][ry].biome == 7:
                rx = randint(0, x - 1)
                ry = randint(0, y - 1)
            
            # Now we can start building our river:
            # Set the first tile to river
            space[rx][ry].biome == 5
            # Get the direction of the movement of the river
            direction = randint(0, 3)
            # Repeat as many times as tiles you want the river to have
            for i in range(0, randint(7, 14)):
                
                # Do the movement, North
                if   direction == 0:
                    # Check for boundaries
                    if ry > 0:
                        ry += -1
                        # There is also a 50% chance of changing the direction
                        if randint(0, 1) == 0:
                            direction = random.choice((1, 3))
                # East
                elif direction == 1:
                    # Check for boundaries
                    if rx < (x - 1):
                        rx += 1
                        # There is also a 50% chance of changing the direction
                        if randint(0, 1) == 0:
                            direction = random.choice((0, 2))
                # South
                elif direction == 2:
                    # Check for boundaries
                    if ry < (y - 1):
                        ry += 1
                        # There is also a 50% chance of changing the direction
                        if randint(0, 1) == 0:
                            direction = random.choice((1, 3))
                # West
                else:
                    # Check for boundaries
                    if rx > 0:
                        rx += -1
                        # There is also a 50% chance of changing the direction
                        if randint(0, 1) == 0:
                            direction = random.choice((0, 2))
                            
                # Now set the current tile to river
                space[rx][ry].biome = 5

                
        # Animals
        print("Designing and spawning animals: 0%", end="\r")
        for i in range(0, x):
            for j in range(0, y):
            
                # There can be from 2 to 5 animals in a cell
                for n in range(0, (2 + randint(0, 3))):
                    # We add some animals to the cell
                    space[i][j].animals.append(random.choice(anDef[space[i][j].biome]))
                    
            # Progress percentage
            print("Designing and spawning animals: " + str(int(i * 100 / x)) + "%", end="\r")
        # Progress percentage for finish
        print("Designing and spawning animals: 100%")

        
        # Plants
        print("Potting and growing plants: 0%", end="\r")
        for i in range(0, x):
            for j in range(0, y):
            
                # There can be from 4 to 10 plants in a cell
                for n in range(0, (4 + randint(0, 6))):
                    # We add some animals to the cell
                    space[i][j].plants.append(random.choice(plDef[space[i][j].biome]))
                    
            # Progress percentage
            print("Potting and growing plants: " + str(int(i * 100 / x)) + "%", end="\r")
        # Progress percentage for finish
        print("Potting and growing plants: 100%")
        
        
        # Temperature
        print("Setting temperatures: 0%", end="\r")
        for i in range(0, x):
            for j in range(0, y):
                # Depending on the biome, the ranges will be different
                # Plains
                if space[i][j].biome == 0:
                    # If we are near the north or south poles
                    if i < (y/10) or i > (y - y/10):
                        space[i][j].temp = randint(10, 15)
                    else:
                        space[i][j].temp = randint(15, 25)
                # Forest
                elif space[i][j].biome == 1:
                    # If we are near the north or south poles
                    if i < (y/10) or i > int(y - y/10):
                        space[i][j].temp = randint(5, 10)
                    else:
                        space[i][j].temp = randint(10, 25)
                # Artic
                elif space[i][j].biome == 2:
                    # Artic will always be in the poles
                    space[i][j].temp = randint(-15, 0)
                # Jungle
                elif space[i][j].biome == 3:
                    # If we are a bit far from the equator
                    if i < (y/4) or i > (y - y/4):
                        space[i][j].temp = randint(15, 25)
                    else:
                        space[i][j].temp = randint(25, 30)
                # Desert
                elif space[i][j].biome == 4:
                    # If we are near the north or south poles
                    if i < (y/10) or i > (y - y/10):
                        space[i][j].temp = randint(25, 35)
                    else:
                        space[i][j].temp = randint(30, 40)
                # River
                elif space[i][j].biome == 5:
                    # If we are a bit far from the equator
                    if i < (y/5) or i > (y - y/5):
                        space[i][j].temp = randint(5, 15)
                    else:
                        space[i][j].temp = randint(15, 20)
                # Mountains
                elif space[i][j].biome == 6:
                    # Here, depending on the height, the ranges vary
                    if space[i][j].height > z - (z / 15):
                        # This is the tallest points of the mountains
                        space[i][j].temp = randint(-10, -5)
                    else:
                        space[i][j].temp = randint(-5, 10)
                # Sea
                else:
                    # If we are a bit far from the equator
                    if i < (y/5) or i > (y - y/5):
                        space[i][j].temp = randint(5, 15)
                    else:
                        space[i][j].temp = randint(15, 25)

                # Progress percentage
                print("Setting temperatures: " + str(int(i * 100 / x)) + "%", end="\r")
         
        # Finish the progress
        print("Setting temperatures: 100%")

        
        # Weather. Some of them have a bigger probability of appearing
        # than others in each biome
        print("Adding and changing weather: 0%", end="\r")
        for i in range(0, x):
            for j in range(0, y):
                # Depending on the biome, the ranges will be different
                # Plains
                if space[i][j].biome == 0:
                    # Clear, cloudy, light rain and rain
                    space[i][j].weather = random.choice((0, 0, 1, 2, 3))
                # Forest
                if space[i][j].biome == 1:
                    # Clear, cloudy, light rain, rain, fog
                    space[i][j].weather = random.choice((0, 1, 1, 2, 3, 8))
                # Artic
                if space[i][j].biome == 2:
                    # Clear, cloudy, snow
                    space[i][j].weather = random.choice((0, 1, 7, 7))
                # Jungle
                if space[i][j].biome == 3:
                    # Light rain, rain, storm, thunderstorm, fog
                    space[i][j].weather = random.choice((2, 2, 3, 4, 5, 8))
                # Desert
                if space[i][j].biome == 4:
                    # Clear, light rain, sandstorm
                    space[i][j].weather = random.choice((0, 0, 2, 9))
                # River
                if space[i][j].biome == 5:
                    # Clear, cloudy, light rain, rain, storm, thunderstorm
                    space[i][j].weather = random.choice((0, 0, 1, 2, 3, 4, 5, 8))
                # Mountains
                if space[i][j].biome == 6:
                    # Clear, cloudy, thunderstorm, snow, fog
                    space[i][j].weather = random.choice((0, 0, 1, 5, 7, 8))
                # Sea
                if space[i][j].biome == 7:
                    # Clear, cloudy, light rain, rain, hurricane, fog
                    space[i][j].weather = random.choice((0, 0, 1, 2, 3, 6, 8))

                # Progress percentage
                print("Adding and changing weather: " + str(int(i * 100 / x)) + "%", end="\r")
         
        # Finish the progress
        print("Adding and changing weather: 100%")
        
        # Cities
        # Initialize the NameGen class
        gen = NameGen()
        for i in range(0, len(space)):
            
            for j in range(0, len(space[i])):
                # For Artic and Mountains biomes there is just 10% chance
                if space[i][j].biome   in (2, 6) and randint(0, 9) == 0:
                    space[i][j].city = City(i, j, gen)
                # For Jungle and Desert, there is a 25% chance
                elif space[i][j].biome in (3, 4) and randint(0, 4) == 0:
                    space[i][j].city = City(i, j, gen)
                # For Plains and Forest there is a 50% chance
                elif space[i][j].biome in (0, 1) and randint(0, 1) == 0:
                    space[i][j].city = City(i, j, gen)
            # For the progress percentage
            print("Building and populating cities: " + str(int(i / x * 100)) + "%", end="\r")

        print("Building and populating cities: 100%")
