class Plant(object):
    def __init__(self, biome):

        # The biome in which the plant is
        self.biome = biome
        # The stats of the plant
        self.name   = ""    # String
        self.energy = 0     # 0 to 5
        self.venomous = False
        # The stuff it returns when harvested
        self.loot   = ""