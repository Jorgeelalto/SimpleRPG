from obj.animal  import Animal
from obj.plant   import Plant
from obj.mineral import Mineral
from sys import platform

class Parser(object):
    def getAnimals(self):

        # We first open the file
        # The path to open depends on the platform this code is running
        # For Windows
        if platform == "win32":
            text = open(r".\def\animals.def", "r")
        # For Unix systems
        else:
            text = open(r"./def/animals.def", "r")
        # We get the string. At this point we have 
        # "1 2 3 4 5 animal1 loot1\n1 2 3 4 5 animal2 loot2\n..."
        s1 = text.read()
        # And split it so we get the separate attributes in each element
        # of the list: ["1 2 3 4 5 animal1 loot1", "1 2 3 4 5 animal2 loot2"...]
        s2 = s1.split(",")
        # We will store all animals types in Animal classes
        # in a list for each biome. Then when we create an actual
        # animal for the world we can just copy the attributes from a
        # random animal in the list
        def0, def1, def2, def3 = [], [], [], []
        def4, def5, def6, def7 = [], [], [], []

        # Now, for each animal definition (item on the list s2), we do:
        for i in s2:

            # Split the definition so we can get the individual values
            s3 = i.split()
            # Create the generic object and set the biome
            a = Animal(int(s3[0]))
            # Set the health
            a.health    = int(s3[1])
            # Set the energy
            a.energy    = int(s3[2])
            # Set the speed
            a.speed     = int(s3[3])
            # Set the power
            a.power     = int(s3[4])
            # Set the dangerous level
            a.dangerous = int(s3[5])
            # Set the name
            a.name      = str(s3[6]).replace("_", " ")
            # And the loot
            a.loot      = str(s3[7]).replace("_", " ")
            # Finally, add the animal template to the corresponding list
            if   a.biome == 0:
                def0.append(a)
            elif a.biome == 1:
                def1.append(a)
            elif a.biome == 2:
                def2.append(a)
            elif a.biome == 3:
                def3.append(a)
            elif a.biome == 4:
                def4.append(a)
            elif a.biome == 5:
                def5.append(a)
            elif a.biome == 6:
                def6.append(a)
            elif a.biome == 7:
                def7.append(a)

        # Return the result of the parsing
        return (def0, def1, def2, def3, def4, def5, def6, def7)
        
    def getPlants(self):

        # We first open the file
        # The path to open depends on the platform this code is running
        # For Windows
        if platform == "win32":
            text = open(r".\def\plants.def", "r")
        # For Unix systems
        else:
            text = open(r"./def/plants.def", "r")
        # We get the string. At this point we have 
        # "1 2 3 4 5 animal1 loot1\n1 2 3 4 5 animal2 loot2\n..."
        s1 = text.read()
        # And split it so we get the separate attributes in each element
        # of the list: ["1 2 3 4 5 animal1 loot1", "1 2 3 4 5 animal2 loot2"...]
        s2 = s1.split(",")
        # We will store all plant types in Plant classes
        # in a list for each biome. Then when we create an actual
        # plant for the world we can just copy the attributes from a
        # random plant in the list
        def0, def1, def2, def3 = [], [], [], []
        def4, def5, def6, def7 = [], [], [], []

        # Now, for each plant definition (item on the list s2), we do:
        for i in s2:

            # Split the definition so we can get the individual values
            s3 = i.split()
            # Create the generic object and set the biome
            p = Plant(int(s3[0]))
            # Set the energy
            p.energy   = int(s3[1])
            # Set whether the plant is venomous or not. Since the Plant
            # constructor sets the default to False, we only have to check
            # for venomous plants
            if s3[2] == "1":
                p.venomous = True
            # Set the name
            p.name     = str(s3[3]).replace("_", " ")
            # Set the loot
            p.loot = str(s3[4])
            # Finally, add the plant template to the corresponding list
            if   p.biome == 0:
                def0.append(p)
            elif p.biome == 1:
                def1.append(p)
            elif p.biome == 2:
                def2.append(p)
            elif p.biome == 3:
                def3.append(p)
            elif p.biome == 4:
                def4.append(p)
            elif p.biome == 5:
                def5.append(p)
            elif p.biome == 6:
                def6.append(p)
            elif p.biome == 7:
                def7.append(p)

        # Return the result of the parsing
        return (def0, def1, def2, def3, def4, def5, def6, def7)
        
        
    def getMinerals(self):

        # We first open the file
        # The path to open depends on the platform this code is running
        # For Windows
        if platform == "win32":
            text = open(r".\def\minerals.def", "r")
        # For Unix systems
        else:
            text = open(r"./def/minerals.def", "r")
        # We get the string. At this point we have 
        # "1 2 3 4 5 animal1 loot1\n1 2 3 4 5 animal2 loot2\n..."
        s1 = text.read()
        # And split it so we get the separate attributes in each element
        # of the list: ["1 2 3 4 5 animal1 loot1", "1 2 3 4 5 animal2 loot2"...]
        s2 = s1.split(",")
        # We will store all plant types in Plant classes
        # in a list for each biome. Then when we create an actual
        # plant for the world we can just copy the attributes from a
        # random plant in the list
        def0, def1, def2, def3 = [], [], [], []
        def4, def5, def6, def7 = [], [], [], []

        # Now, for each plant definition (item on the list s2), we do:
        for i in s2:

            # Split the definition so we can get the individual values
            s3 = i.split()
            # Create the generic object and set the biome
            m = Mineral(int(s3[0]))
            # Set the energy
            m.depth   = int(s3[1])
            # Set the name
            m.name     = str(s3[2]).replace("_", " ")

            # Finally, add the plant template to the corresponding list
            if   m.biome == 0:
                def0.append(m)
            elif m.biome == 1:
                def1.append(m)
            elif m.biome == 2:
                def2.append(m)
            elif m.biome == 3:
                def3.append(m)
            elif m.biome == 4:
                def4.append(m)
            elif m.biome == 5:
                def5.append(m)
            elif m.biome == 6:
                def6.append(m)
            elif m.biome == 7:
                def7.append(m)

        # Return the result of the parsing
        return (def0, def1, def2, def3, def4, def5, def6, def7)
