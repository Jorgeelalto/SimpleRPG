from random import randint
from obj.namegen import NameGen
from obj.npc     import Npc
import random
import time


class City(object):

    def __init__(self, x, y, gen):

        #Cities have a position,
        self.x, self.y = x, y
        # a name, generated from a NameGen instance
        self.name = gen.getCityName(4)
        # and other things that will be here in the future
        # For DEV purposes
        # print("New city: " + self.name + ", on " + str(self.x) + ", " + str(self.y))
        # Add population. There are three different zones, and as you walk around you may
        # have the chance of changing of zone
        self.zoneStreets  = []
        self.zoneMarket   = []
        self.zoneSuburbia = []
        # Population in Streets can vary between 10 and 25 people (WIP)
        for i in range(1, randint(10, 25)):
            self.zoneStreets.append(Npc(gen.getNpcName() ,0))
        # In the Market, there is always between 5 and 10 people (WIP)
        for i in range(1, randint(5, 10)):
            self.zoneMarket.append(Npc(gen.getNpcName(), 1))
        # In the Suburbia, we have between 15 and 30 people (WIP)
        for i in range(1, randint(15, 30)):
            self.zoneSuburbia.append(Npc(gen.getNpcName(), 2))

    def enterCity(self, p):

        print("\n<| Welcome to " + self.name + " |>\n")
        print("You are in the Suburbia of the city.")
        # Boolean to get out of the loop for exiting the city
        inside = True
        # We store the zone in which the player is in any given moment
        # 0 = Streets, 1 = Market, 2 = Suburbia
        self.zone = 2
        # This will determine to which NPC we are talking to
        self.selectedNpc = 0
        
        # CIS loop
        while inside and p.isAlive():

            # Get the player command
            print("")
            cmd = input("|" + p.name + ": ").lower()

            # Exit the game
            if cmd == "exit":
                print("\n<| Thanks for your visit |>\n")
                print("You leave the city.")
                inside = False
   
            # Show some help
            elif cmd == "help":
                print(
                "exit:        leave the city\n" \
                "look:        discover your surroundings\n" \
                "walk away:   change city zone\n" \
                "walk around: move around the zone\n" \
                "talk:        talk with the nearby NPC"
                )

            # Show the player stats
            elif cmd == "stat":
                print(p.getStat())

            # Not the same as walk around: This allows the player to change zone
            elif cmd == "walk away":
                print("You walk away",   end="\r")
                time.sleep(0.8)
                print("You walk away.",  end="\r")
                time.sleep(0.8)
                print("You walk away..", end="\r")
                time.sleep(0.8)
                print("You walk away...")
                
                # Choose a new zone to be changed to 
                new = randint(0, 2)
                # Make sure it is not the zone in which the player currently is
                while new == self.zone:        
                    new = randint(0, 2)
                
                if   new == 0:
                    print("You are now in the Streets.")
                    self.zone = 0
                elif new == 1:
                    print("You are now in the Market.")
                    self.zone = 1
                elif new == 2:
                    print("You are now in the Suburbia.")
                    self.zone = 2


            # Walk around gets you new NPCs to talk to while being on
            # the same zone
            elif cmd == "walk around":
                print("You walk around",   end="\r")
                time.sleep(0.3)
                print("You walk around.",  end="\r")
                time.sleep(0.3)
                print("You walk around..", end="\r")
                time.sleep(0.3)
                print("You walk around...")

                # We do now the NPC change depending on the zone, since
                # the selectedNpc index cannot be larger than the length of
                # the list which stores the NPC in that zone
                if   self.zone == 0:
                    if self.selectedNpc == len(self.zoneSuburbia) - 1:
                        self.selectedNpc = 0
                    else:
                        self.selectedNpc += 1

                elif self.zone == 1:
                    if self.selectedNpc == len(self.zoneMarket) - 1:
                        self.selectedNpc = 0
                    else:
                        self.selectedNpc += 1

                else:
                    if self.selectedNpc == len(self.zoneSuburbia) - 1:
                        self.selectedNpc = 0
                    else:
                        self.selectedNpc += 1

            # Talk to an NPC. You can change to which NPC you will talk by
            # doing a walk around.
            elif cmd == "talk":

                # Streets
                if   self.zone == 0:
                    self.zoneStreets[self.selectedNpc ].talkToNPC(p)
                # Market
                elif self.zone == 1:
                    self.zoneMarket[self.selectedNpc  ].talkToNPC(p)
                # Suburbia
                else:
                    self.zoneSuburbia[self.selectedNpc].talkToNPC(p)
           
            # Take a look around you. Describes the zone and the people who are around.
            elif cmd == "look":

                # Initial string
                s = "You are in the "
                # Adds the corresponding description based on the zone.
                if   self.zone == 0:
                    s = s + "Streets."
                elif self.zone == 1:
                    s = s + "Market."
                elif self.zone == 2:
                    s = s + "Suburbia."
                # When finished adding text, print the string
                print(s)

            # If the command is not valid
            else:
                print("The command is not valid. Try \'help\'.")
