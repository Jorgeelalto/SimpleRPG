#!/usr/bin/python
# -*- encoding: UTF-8 -*-

# Importing the classes
from obj.player import Player
from obj.world  import World
import time


# Game title
print("")
print("▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓")
print("▓▓    ▓▓  ▓▓  ▓▓  ▓▓  ▓▓        ▓▓")
print("▓▓    ▓▓▓▓    ▓▓▓▓▓▓  ▓▓  ▓▓    ▓▓")
print("▓▓    ▓▓  ▓▓  ▓▓      ▓▓  ▓▓    ▓▓")
print("▓▓▓▓  ▓▓  ▓▓  ▓▓      ▓▓▓▓▓▓  ▓▓▓▓")
print("")
print("         by  @jorgeelalto")
print("")
# Creating the world, setting its size
w = World(32, 32, 32)
# Creating the player, and asking for their name
p = Player(str(input("Enter your name, please: ")))
# Set the player coordinates as valid ones, based on the dimension of the world
w.setplayer(p)
# Initializing the main loop
run = True
print("\nType \'help\' to show some guides.")

while run and p.isAlive():
    
    # Enter the Cell. The value it returns works for
    # moving the player around
    cmd = w.space[p.x][p.y].enterCell(p, w)
    
    # Exit the game
    if cmd == "exit":
        run = False
        break
    
    # React to the player's death
    if not p.isAlive():
        # Exit this loop as soon as possible
        print("You are dead.")
        run = False
        break

    # Selector for moving the player to adjacent cells
    if   cmd == "n":
        p.moveUp(w)            
    elif cmd == "s":
        p.moveDown(w)            
    elif cmd == "w":
        p.moveLeft(w)            
    elif cmd == "e":
        p.moveRight(w)
