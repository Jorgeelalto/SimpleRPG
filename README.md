```
▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓▓▓  ▓▓▓▓  
▓▓    ▓▓  ▓▓  ▓▓  ▓▓  ▓▓        ▓▓  
▓▓    ▓▓▓▓    ▓▓▓▓▓▓  ▓▓  ▓▓    ▓▓  
▓▓    ▓▓  ▓▓  ▓▓      ▓▓  ▓▓    ▓▓  
▓▓▓▓  ▓▓  ▓▓  ▓▓      ▓▓▓▓▓▓  ▓▓▓▓  
```  
  
This is a little CLI RPG game written in Python which aims to be expandable and
multiplatform. Currently it runs correctly in Windows, Linux and Android (tested
with the latest build of Python 3 for those platforms and for Termux on
Android).  
  
The game is pretty barebones in terms of user interface, it runs on the terminal
and is designed for interaction with simple keyboard input and commands. On the
other hand, it's designed to be loaded with as much content as possible, adding
new animals for a biome being as simple as writing a line of text, for example.  
  
This is currently under early development and everything is subject to changes.
When it gets to a beta version, it will be released to the public and maybe I
will need some help to get as much balanced content as possible.


# BUILDING FOR DISTRIBUTION

This is Python, it's a scripting language, not designed to compile on anything.
But still, being able to pack the whole game (but the resources) in a single
executable is sweet, even if it's a dirty hack. The "official" version is not
compiled - I develop this program to be run with the interpreter.

For now, I'm experimenting with Nuitka. It compiles the Python code to native
binary equivalent and allows the game to run without the Python runtime installed.
The process requires having Nuitka installed (via pip) and a C++11 compliant C++
compiler. The Nuitka command that compiles successfully the game, executed from
its root directory, is:  
```
nuitka --standalone --recurse-all --python-version=3.6 .\SimpleRPG.py
```  
In Linux, using gcc is enough. For Windows, the recommended and easy method is
installing MinGW32 or 64, depending on your Python version (32 or 64 bits).


After the building is complete, you have to copy the /def directory to /SimpleRPG.dist
so the binary can reach the definitions needed to execute the game.

  
# ROADMAP


This is the planned roadmap with all the Alpha versions until the Beta 1.0 arrives:

### **0.1 (released)**: First Alpha version.  
  
### **0.2 (released)**: World generation update.


More realistic world generation, much more depth to the cells:  

* Add more realistic height and biome distribution, with rivers, artic and mountains, plains and forests, deserts and jungle.  
* Simplified user interface, now there is no Road, you are always in a biome.  
* Added multiple animals and plants to the cells, which are huntable and harvestable, and the possibility
of walking around to look for different things. The player can also dig down and get things from the ground (WIP, teaser for a
future update).  
* The world is correctly circular, no more boundaries.  
* Added temperatures and weather, which has its own cycles for each biome. The weather evolves through time.  
* Smaller fixes and updates comprehend a revamp to the map so the player can only see the part they have explored,
player energy changes, and more possible commands for each action in the cell.
  
  
### **0.3**: Player update.


* Inventory management: throwing away, sorting?, maximum number of items (expandable with a bag?)  
* Decent inventory display, equipped weapon and armor, 
* Crafting, with new definitions for it.  
* All objects can be used for crafting, and there's only one type of object, with all the correspondent stats.
A sword, for example, will have a high attack power but no nutritive value - that is, a metal sword. This opens
up the possibility for silly stuff like a pufferfish sword that you can eat and also inflicts poisonous damage.
  
  
### **0.4**: Combat update.

* Add buffs, inflicted by weapons, tools, consumables and armor. I'll make a list with all possible buffs.  
* A fight overhaul, with the ability to interact with items and stuff.
  
### **0.5**: Cities update.

* Add buildings: churches, shops, homes, and places to sleep.  
* More NPCs in each city.  


### **0.6**: NPCs update.  
Bandits, Traders and Standers with more depth, dialogues,
interactions and special functions.
  
* Bandits may rob, kill, damage or just insult you.
* Traders will try to negotiate with you, or buy or sell stuff with money.
* The standers will tell you useful (or useless) stuff, or they may gift objects.


### **0.7**: Bosses update.  
* Add mini bosses to some cells.
* Add minigames for bosses? 
* Add big bosses that give you special objects or/and powers.


### **0.8**: Content update.  
* Fill the definitions with stuff! Pretty big job, with balancing and all.
* Add loads of dialogues for everything, make them homogeneous.


### **0.9**: GUI update. 
* Add colours.  
* Add progress bars and stuff.  
* Final touches to the dialogues.  
